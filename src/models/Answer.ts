import {Profile} from "src/models/Profil";

export interface IAnswer{
  id: number;
  title: string;
  content: string;
  date: Date;
  profile: Profile;
  state: Boolean;
}

export class Answer implements IAnswer{
  id: number;
  content: string;
  date: Date;
  profile: Profile;
  state: Boolean;
  title: string;

  constructor() {
    this.content = '';
    this.date = new Date();
    this.profile = new Profile();
    this.state = false;
    this.title = '';
    this.id = 0;
  }

}
