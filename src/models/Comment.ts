import {Profile} from "src/models/Profil";
import {Answer} from "src/models/Answer";

export interface IComment{
  id: number;
  title: string;
  content: string;
  date: Date;
  profile: Profile;
  state: Boolean;
  answers: Answer[];
}

export class Comment implements IComment{
  id: number;
  content: string;
  date: Date;
  profile: Profile;
  state: Boolean;
  title: string;
  answers: Answer[];

  constructor() {
    this.content = '';
    this.date = new Date();
    this.profile = new Profile();
    this.state = false;
    this.title = '';
    this.id = 0;
    this.answers = [new Answer()];
  }


}
