import {Profile} from "src/models/Profil";
import {Comment} from "src/models/Comment";
import {Category} from "src/models/Category";

export interface IArticle {
  id: number;
  title: string;
  excerpt: string;
  content: string;
  thumbnail: File;
  profile: Profile;
  comments: Comment[];
  category: Category[];
  date: Date;
}

export class Article implements IArticle{
  thumbnail: File;
  comments: Comment[];
  content: string;
  excerpt: string;
  id: number;
  profile: Profile;
  title: string;
  category: Category[];
  date: Date;

  constructor() {
    this.comments = [new Comment()];
    this.content = '';
    this.excerpt = '';
    this.id = 0
    this.profile = new Profile();
    this.title = '';
    this.thumbnail = null;
    this.date = new Date();
    this.category = [new Category()];
  }

  public toStringCategories() {
    let categories: string = '';
    for (let _i = 0; _i < this.category.length; _i++) {
      categories += this.category[_i].name;
      categories += (_i + 1) != this.category.length ? ',' :'';
      categories += ' ';
    }
    return categories;
  }

}
