export interface IProfile {
  id: number;
  name: string;
  mail: string;
  password: string;
  role: string;
  avatar: File;
}

export class Profile implements IProfile{
  id: number;
  name: string;
  mail: string;
  password: string;
  role: string;
  avatar: File;

  constructor() {
    this.id = 0;
    this.name = '';
    this.mail = '';
    this.password = '';
    this.avatar = null;
  }
}
