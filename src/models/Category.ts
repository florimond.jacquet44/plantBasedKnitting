export interface ICategory{
  id: number;
  name: string;
  code: string;
  color: string;
}

export class Category implements ICategory{
  code: string;
  id: number;
  name: string;
  color: string;

  constructor() {
    this.id= 0;
    this.code = '';
    this.name = '';
    this.color = '';
  }

}
